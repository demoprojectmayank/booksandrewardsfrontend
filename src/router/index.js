import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/pages/Home.vue'
import Login from '../views/auth/Login.vue'
import Logout from '../views/auth/Logout.vue'
import Signup from '../views/auth/Signup.vue'
import UserCoupons from '../views/pages/user/UserCoupons.vue'
import UserBooks from '../views/pages/user/UserBooks.vue'
import Dashboard from '../views/pages/Dashboard.vue';
import BooksStore from '../views/BooksStore.vue'
import Transactions from '../views/pages/user/Transactions.vue';

import AdminCoupons from '../views/pages/admin/AdminCoupons.vue';
import AdminBooks from '../views/pages/admin/AdminBooks.vue';
import AdminUsers from '../views/pages/admin/AdminUsers.vue';
import AddBook from '../views/pages/admin/AddBook.vue';
import AddCoupon from '../views/pages/admin/AddCoupon.vue';

import store from '../store'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { auth: false }
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup,
    meta: { auth: false }
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Logout,
    meta: { auth: true }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: { auth: true }
  },
  {
    path: '/user/books',
    name: 'UserBooks',
    component: UserBooks,
    meta: { auth: true }
  },
  {
    path: '/user/coupons',
    name: 'UserCoupons',
    component: UserCoupons,
    meta: { auth: true }
  },
  {
    path: '/admin/coupons',
    name: 'AdminCoupons',
    component: AdminCoupons,
    meta: { auth: true }
  },
  {
    path: '/admin/coupons',
    name: 'AdminCoupons',
    component: AdminCoupons,
    meta: { auth: true }
  },
  {
    path: '/admin/users',
    name: 'AdminUsers',
    component: AdminUsers,
    meta: { auth: true }
  },
  {
    path: '/admin/books',
    name: 'AdminBooks',
    component: AdminBooks,
    meta: { auth: true }
  },
  {
    path: '/admin/books/add',
    name: 'AddBook',
    component: AddBook,
    meta: { auth: true }
  },
  {
    path: '/admin/coupons/add',
    name: 'AddCoupon',
    component: AddCoupon,
    meta: { auth: true }
  },
  {
    path: '/books/store',
    name: 'BooksStore',
    component: BooksStore,
    meta: { auth: true }
  },
  {
    path: '/user/transactions',
    name: 'Transactions',
    component: Transactions,
    meta: { auth: true }
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.auth === true && !store.getters.isAuth) {
    next('/login');
  }
  else if (to.meta.auth === false && store.getters.isAuth) {
    next('/dashboard');
  }
  else next();
})

export default router
