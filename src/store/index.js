import { createStore } from "vuex";

const store = createStore({
    state() {
        return {
            token: null,
        }
    },
    mutations: {
        setUser(state, { username, token, books, coupons, email, role }) {
            state.username = username;
            state.token = token;
            state.books = books;
            state.coupons = coupons;
            state.email = email;
            state.role = role;
        },
        removeUser(state) {
            state.username = null;
            state.token = null;
        }
    },
    actions: {
        async logIn(context, { username, password }) {
            const baseUrl = process.env.VUE_APP_API_URL;
            const response = await fetch(baseUrl + "authentication/sign-in", {
                method: "POST",
                body: JSON.stringify({
                    username: username,
                    password: password,
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                },
            });
            const data = await response.json();
            if (!response.ok) {
                throw new Error(data.message);
            }
            localStorage.setItem("user_data", JSON.stringify(data));
            context.commit('setUser', data);
        },
        async logOut(context) {
            localStorage.removeItem('user_data');
            context.commit('removeUser');
        },
        async autoLogin(context) {
            let userData = localStorage.getItem('user_data');
            if (userData) {
                try {
                    context.commit('setUser', JSON.parse(userData));
                    const baseUrl = process.env.VUE_APP_API_URL +
                        (context.state.role === 'USER' ? "user/books" : "admin/coupons");
                    const response = await fetch(baseUrl, {
                        method: "GET",
                        headers: {
                            "Authorization": "Bearer " + context.state.token,
                        }
                    });
                    if (!response.ok) throw new Error("Invalid credentials");
                }
                catch (error) {
                    context.dispatch("logOut");
                    window.location.href = "/";
                }
            }
        },
    },
    getters: {
        isAuth: state => state.token != null
    }
})

export default store;
